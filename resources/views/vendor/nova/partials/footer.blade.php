<p class="mt-8 text-center text-xs text-80">
    <a href="https://compwhiz.co.ke" class="text-primary dim no-underline">KMD Data Entry</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Compwhiz LLC - By Kennedy Mutisya, Kennedy Wachira and Dr. Manyali.
    <span class="px-1">&middot;</span>
    v{{ Laravel\Nova\Nova::version() }}
</p>
